<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plant_species".
 *
 * @property string $id
 * @property string $name_ru
 * @property string $name_ua
 * @property string $name_lat
 * @property string $genus_id
 *
 * @property PlantSorts $genus
 * @property Plants[] $plants
 */
class PlantSpecies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plant_species';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_lat', 'genus_id'], 'required'],
            [['genus_id','id'], 'integer'],
            [['name_ru', 'name_ua', 'name_lat'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_ru' => Yii::t('app', 'plants.species.name_ru'),
            'name_ua' => Yii::t('app', 'plants.species.name_ua'),
            'name_lat' => Yii::t('app', 'plants.species.name_lat'),
            'genus_id' => Yii::t('app', 'plants.species.genus_id'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenus()
    {
        return $this->hasOne(PlantSorts::className(), ['id' => 'genus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlants()
    {
        return $this->hasMany(Plants::className(), ['species_id' => 'id']);
    }

    public static  function getPlantSpeciesByName($plantSpecieName){

        return PlantSpecies::find()
            ->where(['name_lat'=>$plantSpecieName])
            ->orWhere(['name_ru'=>$plantSpecieName])
            ->orWhere(['name_ua'=>$plantSpecieName])
            ->one();
    }
}
