<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * ItemsSearch represents the model behind the search form about `common\models\Items`.
 */
class ItemsSearch extends Items
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nursery_id', 'height_min', 'height_max', 'width_min', 'width_max', 'girth_min', 'girth_max', 'shtamb', 'amount', 'created_at', 'updated_at'], 'integer'],
            [['landing_type'], 'safe'],
            [['price', 'price_ds1', 'price_ds2', 'price_retail'], 'number'],
            ['plant_id', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Items::find()->select('items.*,nurseries.name,product.name_lat')
                 ->leftJoin('nurseries', '`nurseries`.`id` = `items`.`nursery_id`')
                 ->leftJoin('product', '`product`.`id` = `items`.`plant_id`')
                 ->with('nurseries')
                 ->with('product');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'plant_id' => $this->plant_id,
            'nursery_id' => $this->nursery_id,
            'height_min' => $this->height_min,
            'height_max' => $this->height_max,
            'width_min' => $this->width_min,
            'width_max' => $this->width_max,
            'girth_min' => $this->girth_min,
            'girth_max' => $this->girth_max,
            'shtamb' => $this->shtamb,
            'amount' => $this->amount,
            'price' => $this->price,
            'price_ds1' => $this->price_ds1,
            'price_ds2' => $this->price_ds2,
            'price_retail' => $this->price_retail,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'landing_type', $this->landing_type]);

        return $dataProvider;
    }
}
