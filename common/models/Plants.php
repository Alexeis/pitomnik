<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plants".
 *
 * @property string $id
 * @property string $sort_id
 * @property string $note
 * @property string $cultivar
 * @property string $species_id
 * @property string $class_id
 *
 * @property Items[] $items
 * @property PlantClasses $class
 * @property PlantSpecies $species
 * @property PlantSorts $sort
 * @property StatItems[] $statItems
 */
class Plants extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort_id'], 'required'],
            [['sort_id', 'species_id', 'class_id'], 'integer'],
            [['note'], 'string'],
            [['cultivar'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sort_id' => Yii::t('app', 'Sort ID'),
            'note' => Yii::t('app', 'Note'),
            'cultivar' => Yii::t('app', 'Cultivar'),
            'species_id' => Yii::t('app', 'Species ID'),
            'class_id' => Yii::t('app', 'Class ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['plant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(PlantClasses::className(), ['id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecies()
    {
        return $this->hasOne(PlantSpecies::className(), ['id' => 'species_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSort()
    {
        return $this->hasOne(PlantSorts::className(), ['id' => 'sort_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatItems()
    {
        return $this->hasMany(StatItems::className(), ['plant_id' => 'id']);
    }
}
