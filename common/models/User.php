<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property array $auth_assignment
 * @property string $created_password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_APPROVED = 20; //approved by admin user as a nursery owner


    // role assignment array
    public $auth_assignment;
    // assigned by admin password
    public $created_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['username', 'auth_key', 'password_hash', 'email'], 'required'],
        	[['status'], 'integer'],
        	[['email'],'email'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_DELETED,  self::STATUS_APPROVED]],
            [['auth_assignment'], 'safe'],
            [['created_password'],'required','on' => 'creating_manually'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['creating_manually'] = ['username','email','created_password','status'];//Scenario Values Only Accepted
        return $scenarios;
    }
    /**
     * Overriding attributeLabels with I18N
     * @see \yii\base\Model::attributeLabels()
     */
    public function attributeLabels()
    {
    	return [
    			'id' => Yii::t('app', 'user.user_id'),
    			'username' => Yii::t('app', 'user.username'),
    			'email' => Yii::t('app', 'user.email'),
    			'created_at' => Yii::t('app', 'user.created_at'),
    			'updated_at' => Yii::t('app', 'user.updated_at'),
    			'status' => Yii::t('app', 'user.status'),
                'auth_assignment' => Yii::t('app', 'user.auth.assignment'),
                'created_password' => Yii::t('app', 'user.created.password'),
    	];
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()->where(['id' => $id, 'status' => self::STATUS_ACTIVE])
                             ->orWhere(['status' => self::STATUS_APPROVED])
                             ->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {

        return static::find()->where(['username' => $username, 'status' => self::STATUS_ACTIVE])
                             ->orWhere(['status' => self::STATUS_APPROVED])
                             ->one();
    }
    
    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
    	return static::find()->where(['email' => $email, 'status' => self::STATUS_ACTIVE])
                             ->orWhere(['status' => self::STATUS_APPROVED])
                             ->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::find()->where(['password_reset_token' => $token,'status' => self::STATUS_ACTIVE])
                             ->orWhere(['status' => self::STATUS_APPROVED])
                             ->one();
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    public static function getNursery($user_id = null)
    {
    	return Nurseries::findOne(['user_id' => $user_id]);
    }

    public function getAuthAssignment()
    {
        $rbac = \Yii::$app->authManager;
        return $rbac->getAssignments($this->getId());
    }


    public function afterSave($insert, $changedAttributes){

        //update rbac roles
        if ($this->auth_assignment) {
            $rbac = \Yii::$app->authManager;
            $rbac->revokeAll($this->getId());
            foreach ($this->auth_assignment as $aa) { //Write new values
                $role = $rbac->getRole($aa);
                $rbac->assign($role, $this->getId());
            }
        }
    }

}
