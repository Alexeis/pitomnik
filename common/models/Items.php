<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "items".
 *
 * @property integer $id
 * @property integer $plant_id
 * @property integer $nursery_id
 * @property string $landing_type
 * @property integer $height_min
 * @property integer $height_max
 * @property integer $width_min
 * @property integer $width_max
 * @property integer $girth_min
 * @property integer $girth_max
 * @property integer $shtamb
 * @property integer $amount
 * @property double $price
 * @property double $price_ds1
 * @property double $price_ds2
 * @property double $price_retail
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Nurseries $nursery
 * @property Plants $plant
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plant_id', 'nursery_id'], 'required'],
            [['plant_id', 'nursery_id', 'height_min', 'height_max', 'width_min', 'width_max', 'girth_min', 'girth_max', 'shtamb', 'amount'], 'integer'],
            [['price', 'price_ds1', 'price_ds2', 'price_retail'], 'number'],
            [['landing_type'], 'string', 'max' => 15]
        ];
    }

    /**
     * Autofill created_at and updated_at fields
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'plant_id' => Yii::t('app', 'Plant ID'),
            'nursery_id' => Yii::t('app', 'Nursery ID'),
            'landing_type' => Yii::t('app', 'Landing Type'),
            'height_min' => Yii::t('app', 'Height Min'),
            'height_max' => Yii::t('app', 'Height Max'),
            'width_min' => Yii::t('app', 'Width Min'),
            'width_max' => Yii::t('app', 'Width Max'),
            'girth_min' => Yii::t('app', 'Girth Min'),
            'girth_max' => Yii::t('app', 'Girth Max'),
            'shtamb' => Yii::t('app', 'Shtamb'),
            'amount' => Yii::t('app', 'Amount'),
            'price' => Yii::t('app', 'Price'),
            'price_ds1' => Yii::t('app', 'Price Ds1'),
            'price_ds2' => Yii::t('app', 'Price Ds2'),
            'price_retail' => Yii::t('app', 'Price Retail'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNurseries()
    {
        return $this->hasOne(Nurseries::className(), ['id' => 'nursery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlant()
    {
        return $this->hasOne(Plants::className(), ['id' => 'plant_id']);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'plant_id']);

    }
}
