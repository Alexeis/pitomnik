<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plant_classes".
 *
 * @property integer $id
 * @property string $name_lat
 * @property string $name_ru
 * @property string $name_ua
 *
 * @property PlantSorts[] $plantSorts
 */
class PlantClasses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plant_classes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_lat'], 'required'],
            [['name_lat', 'name_ru', 'name_ua'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_lat' => Yii::t('app', 'Name Lat'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_ua' => Yii::t('app', 'Name Ua'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantSorts()
    {
        return $this->hasMany(PlantSorts::className(), ['class_id' => 'id']);
    }

    public static function getPlantClassByName($sortName){

        return PlantClasses::find()->where(['name_lat'=>$sortName])->orWhere(['name_ru'=>$sortName])->orWhere(['name_ua'=>$sortName])->one();
    }
}
