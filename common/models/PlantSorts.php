<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plant_sorts".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_ua
 * @property string $name_lat
 * @property integer $class_id
 *
 * @property PlantClasses $class
 * @property Plants[] $plants
 */
class PlantSorts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plant_sorts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'class_id'], 'required'],
            [['class_id'], 'integer'],
            [['name_ru', 'name_ua', 'name_lat'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_ua' => Yii::t('app', 'Name Ua'),
            'name_lat' => Yii::t('app', 'Name Lat'),
            'class_id' => Yii::t('app', 'Class ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(PlantClasses::className(), ['id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlants()
    {
        return $this->hasMany(Plants::className(), ['sort_id' => 'id']);
    }
}
