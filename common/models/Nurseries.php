<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "nurseries".
 *
 * @property integer $id
 * @property string $name
 * @property integer $region_id
 * @property string $address
 * @property string $phone
 * @property string $site_url
 * @property string $note
 * @property integer $user_id
 * @property string $profile
 *
 * @property Items[] $items
 * @property User $user
 * @property Regions $region
 * @property StatItems[] $statItems
 */
class Nurseries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nurseries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'region_id', 'user_id'], 'required'],
            [['region_id', 'user_id'], 'integer'],
            [['note'], 'string'],
            [['name', 'site_url'], 'string', 'max' => 150],
            [['address', 'profile'], 'string', 'max' => 300],
            [['phone'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'nursery.ID'),
            'name' => Yii::t('app', 'nursery.Name'),
            'region_id' => Yii::t('app', 'nursery.Region ID'),
            'address' => Yii::t('app', 'nursery.Address'),
            'phone' => Yii::t('app', 'nursery.Phone'),
            'site_url' => Yii::t('app', 'nursery.Site Url'),
            'note' => Yii::t('app', 'nursery.Note'),
            'user_id' => Yii::t('app', 'nursery.User ID'),
            'profile' => Yii::t('app', 'nursery.Profile'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['nursery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatItems()
    {
        return $this->hasMany(StatItems::className(), ['nursery_id' => 'id']);
    }
}
