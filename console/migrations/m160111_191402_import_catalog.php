<?php

use yii\db\Schema;
use yii\db\Migration;
//use common\models\Product;
use common\models\Plants;
use common\models\PlantClasses;
use common\models\PlantSorts;
use common\models\PlantSpecies;
use common\models\Nurseries;
use creocoder\nestedsets\NestedSetsBehavior;
use creocoder\nestedsets\NestedSetsQueryBehavior;

class Product extends \yii\db\ActiveRecord
{
    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                 'treeAttribute' => 'root',
                // 'leftAttribute' => 'lft',
                // 'rightAttribute' => 'rgt',
                 'depthAttribute' => 'lvl',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
}
class ProductQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}

class m160111_191402_import_catalog extends Migration
{
    public function up()
    {
        Product::deleteAll();
        foreach(PlantClasses::find()->all() as $class){
            $product = new Product();
            $product->id = $class->id;
            $product->name_lat = $class->name_lat;
            $product->name_ru = $class->name_ru;
            $product->name_ua = $class->name_ua;
            $product->collapsed = 1;
            $product->makeRoot();
        }
        foreach(PlantSorts::find()->all() as $sort) {
            $product = new Product();
            $product->id = $sort->id + 3;
            $product->name_lat = $sort->name_lat;
            $product->name_ru = $sort->name_ru;
            $product->name_ua = $sort->name_ua;
            $product->collapsed = 1;
            $father = Product::find()->where(['id' => $sort->class_id])->one();
            $product->appendTo($father);
        }
        foreach(PlantSpecies::find()->all() as $specie) {
            $product = new Product();
            $product->id = $specie->id + 20;
            $product->name_lat = $specie->name_lat;
            $product->name_ru = $specie->name_ru;
            $product->name_ua = $specie->name_ua;
            $product->collapsed = 1;
            $father = Product::find()->where(['id' => $specie->genus_id + 3])->one();
            $product->appendTo($father);
        }

        foreach(Plants::find()->all() as $plant) {
            $product = new Product();
            $product->id = $plant->id + 116;
            if ($plant->cultivar) {
                $product->name_lat = $plant->cultivar;
                $product->name_ru = $plant->cultivar;
                $product->name_ua = $plant->cultivar;
            }
            else{
                $product->name_lat = 'безсортовый';
                $product->name_ru = 'безсортовый';
                $product->name_ua = 'безсортовый';
            }
            $product->collapsed = 1;
            $father = Product::find()->where(['id' => $plant->species_id + 20])->one();
            $product->appendTo($father);
        }

        //$product = new Product(['name_lat' => 'flkjfklsdjfkldsjf','name_ru' => 'Русское']);
        //$product->makeRoot();

    }

    public function down()
    {
        Product::deleteAll();
        echo " Table Product was cleared\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
