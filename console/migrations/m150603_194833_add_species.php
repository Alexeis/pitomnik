<?php

use yii\db\Schema;
use yii\db\Migration;

class m150603_194833_add_species extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('plant_species', [
            'id' => Schema::TYPE_PK,
            'name_ru' => Schema::TYPE_STRING . ' NOT NULL',
            'name_ua' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'name_lat' => Schema::TYPE_STRING . ' NOT NULL',
            'genus_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
        ], $tableOptions);

        $this->createIndex('ind_genus_id','plant_species','genus_id');


        $this->execute('
            insert into plant_species (name_lat,name_ru,name_ua,genus_id)
              select distinct name_lat,name_ru,name_ua,sort_id from plants;
        ');

        $this->addForeignKey('fk_ps_genus','plant_species','genus_id','plant_sorts','id','CASCADE','CASCADE');
        $this->addColumn('plants','species_id', Schema::TYPE_INTEGER);

        $this->execute('
            update plants p set
              p.species_id= (select max(s.id) from plant_species s where s.name_lat = p.name_lat);
        ');

        $this->addColumn('plants','class_id', Schema::TYPE_INTEGER. ' UNSIGNED');

        $this->execute('
            update plants p set
              class_id = (select max(s.class_id) from plant_sorts s where s.id=p.sort_id);
        ');

        $this->dropColumn('plants','name_lat');
        $this->dropColumn('plants','name_ru');
        $this->dropColumn('plants','name_ua');

        $this->createIndex('ind_plant_class','plants','class_id');
        $this->createIndex('ind_plant_species','plants','species_id');

        $this->addForeignKey('fk_plants_class','plants','class_id','plant_classes','id','CASCADE','CASCADE');
        $this->addForeignKey('fk_plants_species','plants','species_id','plant_species','id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->addColumn('plants','name_lat',Schema::TYPE_STRING);
        $this->addColumn('plants','name_ru',Schema::TYPE_STRING);
        $this->addColumn('plants','name_ua',Schema::TYPE_STRING);

        $this->execute('
            update plants p set
              p.name_lat = (select s.name_lat from plant_species s where s.id=p.species_id);
        ');
        $this->execute('
            update plants p set
              p.name_ru = (select s.name_ru from plant_species s where s.id=p.species_id);
        ');
        $this->execute('
            update plants p set
              p.name_ua = (select s.name_ua from plant_species s where s.id=p.species_id);
        ');

        $this->dropForeignKey('fk_plants_class','plants');
        $this->dropForeignKey('fk_plants_species','plants');
        $this->dropIndex('ind_plant_class','plants');
        $this->dropIndex('ind_plant_species','plants');
        $this->dropColumn('plants','species_id');
        $this->dropColumn('plants','class_id');
        $this->dropForeignKey('fk_ps_genus','plant_species');
        $this->dropTable('plant_species');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
