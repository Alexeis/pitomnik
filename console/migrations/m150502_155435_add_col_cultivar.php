<?php

use yii\db\Schema;
use yii\db\Migration;

class m150502_155435_add_col_cultivar extends Migration
{
    public function up()
    {
        $this->execute('
                ALTER TABLE `plants`
                ADD `cultivar` varchar(100) DEFAULT NULL;
        ');
    }

    public function down()
    {
        $this->dropColumn('plants','cultivar');
    }
    
}
