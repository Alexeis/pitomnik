<?php

use yii\db\Migration;

class m150531_091451_rbac_fix extends Migration
{
    public function safeUp()
    {
        $rbac = \Yii::$app->authManager;
        $rbac->removeAll();

        $holder = $rbac->createRole('holder');
        $holder->description = 'Nursery owner';
        $rbac->add($holder);

        $admin = $rbac->createRole('administrator');
        $admin->description = 'Site Admin';
        $rbac->add($admin);

        $rbac->addChild($admin, $holder);

        $perm = $rbac->createPermission('importPrice');
        $perm->description = 'Allows to import own price';
        $rbac->add($perm);
        $rbac->addChild($rbac->getRole('holder'),$perm);

        /**
         * assign admin to user you want
         */
        /*
        $rbac->assign(
            $admin,
            \common\models\User::findOne(['username' =>
                'all2001'])->id
        );
        */

    }

    public function safeDown()
    {
        $manager = \Yii::$app->authManager;
        $manager->removeAll();
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
