<?php

use yii\db\Migration;
use creocoder\nestedsets\NestedSetsBehavior;
use creocoder\nestedsets\NestedSetsQueryBehavior;



class Product extends \yii\db\ActiveRecord
{
    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'root',
                // 'leftAttribute' => 'lft',
                // 'rightAttribute' => 'rgt',
                'depthAttribute' => 'lvl',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
}
class ProductQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}


class m160309_133315_import_new_catalog extends Migration
{
    public function up()

    {

        /* Format of xlsx file:
         *
         * class_name_ru	rod_name_lat	rod_name_ru	rod_name_ua	vid_name_lat	vid_name_ru	vid_name_ua	sort_name_lat
         *
         *
         *  */

        $inputFileName = 'console/runtime/spr.xlsx';
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $errors = [];
        Product::deleteAll();
        for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                FALSE,
                FALSE);

            $class_name_ru = $rowData[0][0];
            $rod_name_lat = $rowData[0][1];
            $rod_name_ru = $rowData[0][2];
            $rod_name_ua = $rowData[0][3];
            $vid_name_lat = $rowData[0][4];
            $vid_name_ru = $rowData[0][5];
            $vid_name_ua = $rowData[0][6];
            $sort_name_lat = (trim($rowData[0][7]) != '') ? $rowData[0][7] : 'безсортовый' ;
            /*классы*/
            if (trim($class_name_ru) != ''){
                if (!Product::find()->where(['name_ru' => $class_name_ru])->one()){
                    $product = new Product();
                    $product->name_lat = $class_name_ru;
                    $product->name_ru = $class_name_ru;
                    $product->name_ua = $class_name_ru;
                    $product->collapsed = 1;
                    $product->makeRoot();
                }
            }
            else{
                $errors[] = $row.' строка пустое значение класса';
                continue;
            }

            /*роды*/
            if (trim($rod_name_lat) != ''){
                if (!Product::find()->where(['name_lat' => $rod_name_lat])->one()) {
                    $father = Product::find()->where(['name_ru' => $class_name_ru])->one();
                    $product = new Product();
                    $product->name_lat = $rod_name_lat;
                    $product->name_ru = $rod_name_ru;
                    $product->name_ua = $rod_name_ua;
                    $product->collapsed = 1;
                    $product->appendTo($father);
                }

            }
            else{
                $errors[] = $row.' строка пустое значение рода';
                continue;
            }

            /*виды*/
            if (trim($vid_name_lat) != '')
            {
                if (!Product::find()->where(['name_lat' => $vid_name_lat])->one())
                {
                    $father = Product::find()->where(['name_lat' => $rod_name_lat])->one();
                    $product = new Product();
                    $product->name_lat = $vid_name_lat;
                    $product->name_ru = $vid_name_ru;
                    $product->name_ua = $vid_name_ua;
                    $product->collapsed = 1;
                    $product->appendTo($father);
                }

            }
            else{
                $errors[] = $row.' строка пустое значение вида';
                continue;
            }

            /*сорт*/
            if (trim($sort_name_lat) != '')
            {
                $father = Product::find()->where(['name_lat' => $vid_name_lat])->one();
                $product = new Product();
                $product->name_lat = $sort_name_lat;
                $product->name_ru = $sort_name_lat;
                $product->name_ua = $sort_name_lat;
                $product->appendTo($father);

            }

        }
        return true;


    }

    public function down()
    {
        Product::deleteAll();
        echo " Table Product was cleared\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
