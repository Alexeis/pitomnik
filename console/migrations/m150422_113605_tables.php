<?php

use yii\db\Migration;

class m150422_113605_tables extends Migration
{
    public function up()
    {

    $sql = "

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) unsigned NOT NULL,
  `plant_id` int(11) unsigned NOT NULL COMMENT 'Растение',
  `nursery_id` int(11) NOT NULL COMMENT 'Питомник',
  `landing_type` varchar(15) DEFAULT NULL COMMENT 'Тип посадки',
  `height_min` smallint(4) unsigned DEFAULT NULL COMMENT 'Минимальная высота',
  `height_max` smallint(4) DEFAULT NULL COMMENT 'Максимальная высота',
  `width_min` smallint(4) unsigned DEFAULT NULL COMMENT 'Минимальная ширина',
  `width_max` smallint(4) unsigned DEFAULT NULL COMMENT 'Максимальная ширина',
  `girth_min` smallint(4) unsigned DEFAULT NULL COMMENT 'Минимальный обхват',
  `girth_max` smallint(4) unsigned DEFAULT NULL COMMENT 'Максимальный обхват',
  `shtamb` smallint(4) DEFAULT NULL COMMENT 'Штамб',
  `amount` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Количество',
  `price` double(11,2) NOT NULL DEFAULT '0.00' COMMENT 'Цена',
  `price_ds1` double(11,2) NOT NULL DEFAULT '0.00' COMMENT 'Цена для садовых центров',
  `price_ds2` double(11,2) NOT NULL DEFAULT '0.00' COMMENT 'Цена для ландшафтников',
  `price_retail` double(11,2) NOT NULL DEFAULT '0.00' COMMENT 'Розничная цена',
  `created_at` int(11) DEFAULT NULL COMMENT 'Дата добавления в каталог',
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `header` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `nurseries` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL COMMENT 'Название питомника',
  `region_id` int(11) NOT NULL COMMENT 'Область',
  `address` varchar(300) DEFAULT NULL COMMENT 'Адрес питомника',
  `phone` varchar(100) DEFAULT NULL COMMENT 'Телефоны',
  `site_url` varchar(150) DEFAULT NULL COMMENT 'Сайт питомника',
  `note` text COMMENT 'Краткое описание питомника',
  `user_id` int(11) NOT NULL,
  `profile` varchar(300) DEFAULT NULL COMMENT 'Специализация питомника'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `plants` (
  `id` int(11) unsigned NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `name_ua` varchar(255) DEFAULT NULL,
  `name_lat` varchar(255) NOT NULL,
  `sort_id` int(11) unsigned NOT NULL COMMENT 'Вид растения',
  `note` text
) ENGINE=InnoDB AUTO_INCREMENT=360 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `plant_classes` (
  `id` int(11) unsigned NOT NULL,
  `name_lat` varchar(255) NOT NULL,
  `name_ru` varchar(255) DEFAULT NULL,
  `name_ua` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='classes of plants';


CREATE TABLE IF NOT EXISTS `plant_sorts` (
  `id` int(10) unsigned NOT NULL,
  `name_ru` varchar(255) NOT NULL COMMENT 'Наименование вида растения по русски',
  `name_ua` varchar(255) DEFAULT NULL COMMENT 'Наименование вида растения по украински',
  `name_lat` varchar(255) DEFAULT NULL COMMENT 'Наименование вида растения по английски',
  `class_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `stat_items` (
  `id` int(11) unsigned NOT NULL,
  `plant_id` int(11) unsigned NOT NULL COMMENT 'Растение',
  `nursery_id` int(11) NOT NULL COMMENT 'Питомник',
  `landing_type` varchar(15) DEFAULT NULL COMMENT 'Тип посадки',
  `height_min` smallint(4) unsigned DEFAULT NULL COMMENT 'Минимальная высота',
  `height_max` smallint(4) DEFAULT NULL COMMENT 'Максимальная высота',
  `width_min` smallint(4) unsigned DEFAULT NULL COMMENT 'Минимальная ширина',
  `width_max` smallint(4) unsigned DEFAULT NULL COMMENT 'Максимальная ширина',
  `girth_min` smallint(4) unsigned DEFAULT NULL COMMENT 'Минимальный обхват',
  `girth_max` smallint(4) unsigned DEFAULT NULL COMMENT 'Максимальный обхват',
  `shtamb` smallint(4) DEFAULT NULL COMMENT 'Штамб',
  `amount` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Количество',
  `price` double(11,2) NOT NULL DEFAULT '0.00' COMMENT 'Цена',
  `price_ds1` double(11,2) NOT NULL DEFAULT '0.00' COMMENT 'Цена для садовых центров',
  `price_ds2` double(11,2) NOT NULL DEFAULT '0.00' COMMENT 'Цена для ландшафтников',
  `price_retail` double(11,2) NOT NULL DEFAULT '0.00' COMMENT 'Розничная цена',
  `created_at` int(11) DEFAULT NULL COMMENT 'Дата добавления в каталог',
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `items`
  ADD PRIMARY KEY (`id`), ADD KEY `ind_its_plant` (`plant_id`), ADD KEY `ind_its_nursery` (`nursery_id`);

ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ind_news_user` (`user_id`);


ALTER TABLE `nurseries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ind_nrs_region` (`region_id`),
  ADD KEY `ind_nsr_users` (`user_id`);

ALTER TABLE `plants`
  ADD PRIMARY KEY (`id`), ADD KEY `ind_plant1` (`sort_id`);

ALTER TABLE `plant_classes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `plant_sorts`
  ADD PRIMARY KEY (`id`), ADD KEY `ind_ps_class` (`class_id`);

ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `stat_items`
  ADD PRIMARY KEY (`id`), ADD KEY `ind_si_plant` (`plant_id`), ADD KEY `ind_si_nursery` (`nursery_id`);

ALTER TABLE `items`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `nurseries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `plants`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=360;

ALTER TABLE `plant_classes`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `plant_sorts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `stat_items`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `items`
ADD CONSTRAINT `fk_its_nursery` FOREIGN KEY (`nursery_id`) REFERENCES `nurseries` (`id`),
ADD CONSTRAINT `fk_its_plant` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`);

ALTER TABLE `nurseries`
ADD CONSTRAINT `fk_nrs_region` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`);


ALTER TABLE `plants`
ADD CONSTRAINT `fk_pl_sorts` FOREIGN KEY (`sort_id`) REFERENCES `plant_sorts` (`id`);


ALTER TABLE `plant_sorts`
ADD CONSTRAINT `fk_ps_classes` FOREIGN KEY (`class_id`) REFERENCES `plant_classes` (`id`);

ALTER TABLE `stat_items`
ADD CONSTRAINT `fk_sits_nursery` FOREIGN KEY (`nursery_id`) REFERENCES `nurseries` (`id`),
ADD CONSTRAINT `fk_sits_plant` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`);

ALTER TABLE `news` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
ALTER TABLE `nurseries` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
";
        $queries = explode(';',$sql);

        foreach($queries as $sql){
            if(trim($sql)){
                $this->execute($sql);
            }
        }
    }

    public function down()
    {



        $sql = "

DROP TABLE `items`;

DROP TABLE `news`;

DROP TABLE `stat_items`;

DROP TABLE `plants`;

DROP TABLE `nurseries`;

DROP TABLE `regions`;

DROP TABLE `plant_sorts`;

DROP TABLE `plant_classes`;

";

        $queries = explode(';',$sql);

        foreach($queries as $sql){
            if(trim($sql)){
                $this->execute($sql);
            }
        }

    }
}
