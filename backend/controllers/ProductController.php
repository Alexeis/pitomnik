<?php

namespace backend\controllers;

use Yii;
use yii\web\UploadedFile;
use common\models\Product;

class ProductController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public  function actionFileUpload(){

        if (Yii::$app->request->isPost) {
            $request = Yii::$app->request->post();
            $model = Product::findOne(['id' => $request['node_id']]);
            $uploadedFile = UploadedFile::getInstance($model, 'file');
            $uploadedFile->saveAs(Yii::getAlias('@webroot').'/uploads/'.$uploadedFile->name);
            $model->plant_foto = $uploadedFile->name;
            $model->save();
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['data' => 'success'];
        return $result;
    }

}
