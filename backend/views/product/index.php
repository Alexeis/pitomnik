<?php
/* @var $this yii\web\View */
use common\models\Product;
use kartik\tree\TreeView;
?>
<?php
    $query = Product::find()->andFilterWhere(['like', 'name_lat', $name_lat])->addOrderBy('root, lft');
    echo TreeView::widget([
        'query' => $query,
        'headingOptions' => ['label' => 'Растения'],
        'nodeView' => '@backend/views/product/_form',
        'rootOptions' => ['label'=>'<span class="text-primary">Root</span>'],
        'options' => [
            'id' => 'myTree',
        ],
        'fontAwesome' => false,
        'isAdmin' => true,
        'displayValue' => 1,
        'softDelete' => false,
        'showInactive' => false,
        'cacheSettings' => ['enableCache' => true]
    ]);
?>

