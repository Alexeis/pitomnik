<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="'../images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>



<div >
    <?php

    if (!Yii::$app->user->isGuest) {
        NavBar::begin([
            'brandLabel' => Yii::t('app', 'Pitomnik'),
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-default navbar-fixed-top navbar-inverse',
            ],
            'innerContainerOptions' => [
                'class' => 'container-fluid',
            ]
        ]);


        $menuItems = [];

        $menuItems[] = ['label' => '', 'url' => ['#', 'class' => 'glyphicon glyphicon-menu-hamburger', 'options' => [ 'id' => 'toggle-side']], ];


        $menuItems[] = ['label' => Yii::t('app', 'nav.test'), 'url' => ['/catalog/test']];

        $menuItems[] = ['label' => Yii::t('app', 'nav.search'), 'url' => ['/catalog/search']];

        $menuItems[] = ['label' => Yii::t('app', 'nav.import'), 'url' => ['/catalog/import']];

        $menuItems[] = ['label' => Yii::t('app', 'nav.settings'),
            'options' => ['style' => 'dropdown'],
            'items' => [
                ['label' => Yii::t('app', 'nav.settings.profile'), 'url' => '/profile/edit'],
                ['label' => Yii::t('app', 'nav.settings.news'), 'url' => '#'],
            ]
        ];

        $menuItems[] = [
            'label' => Yii::$app->user->identity->username . '(' . (\Yii::$app->user->can('administrator') ? 'admin' : 'user') . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
    }


    ?>


    <div id="wrapper" class="<?= Yii::$app->user->isGuest ? 'toggled' : '' ?>">


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">

                <div class="top-menu-push"></div>

                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>


                <?= $content ?>

            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::t('view','site.name') ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
