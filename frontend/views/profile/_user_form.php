
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Regions;
use common\models\User;
use yii\helpers\ArrayHelper;

$form = ActiveForm::begin();

?>

<?= $form->field($model, 'username', [
    'inputOptions' => ['autocomplete' => 'off', 'class' => 'form-control'],
]) ?>


<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>

<?php
ActiveForm::end();