<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Regions;
use common\models\User;
use yii\helpers\ArrayHelper;

$form = ActiveForm::begin();

?>

<?= $form->field($model, 'name') ?>

<?=
$form->field($model, 'region_id')->dropDownList(
    ArrayHelper::map(Regions::find()->all(), 'id', 'name'),
    [
        'inputOptions' => ['autocomplete' => 'off', 'class' => 'form-control'],
    ]
)
?>

<?= $form->field($model, 'user_id')->hiddenInput() ?>

<?= $form->field($model, 'address', [
    'inputOptions' => ['autocomplete' => 'off', 'class' => 'form-control'],
]) ?>

<?=
$form->field($model, 'phone', [
    'inputOptions' => ['autocomplete' => 'off', 'class' => 'form-control'],
])->textInput()
?>
<?=
$form->field($model, 'site_url', [
    'inputOptions' => ['autocomplete' => 'off', 'class' => 'form-control'],
])->textInput()
?>
<?=
$form->field($model, 'note', [
    'inputOptions' => ['autocomplete' => 'off', 'class' => 'form-control'],
])->textArea()
?>
<?=
$form->field($model, 'profile', [
    'inputOptions' => ['autocomplete' => 'off', 'class' => 'form-control'],
])->textInput()
?>


<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>

<?php
    ActiveForm::end();

