<?php

use yii\bootstrap\Tabs;
use kartik\widgets\Alert;


$this->title = Yii::t('app', 'profile.profile');
$this->params['breadcrumbs'][] = $this->title;




if (\Yii::$app->session->hasFlash('success')) {
    echo Alert::widget([
        'type' => Alert::TYPE_SUCCESS,
        'title' => Yii::t('app', 'profile.update.success.title'),
        'icon' => 'glyphicon glyphicon-ok-sign',
        'body' => \Yii::$app->session->getFlash('success'),
        'showSeparator' => true,
        'delay' => 3000
    ]);
}

?>

<div class="row">
    <div class="col-md-12">
<?=

Tabs::widget([
    'items' => [
        [
            'label' => Yii::t('app', 'profile.nurseryinfo'),
            'content' => $this->render('_nursery_form', ['model' => $nursery], true),
            'options' => ['id' => 'nurseryinfo'],
            'active' => !$isProfile
        ],
        [
            'label' => Yii::t('app', 'profile.userinfo'),
            'content' => $this->render('_user_form', ['model' => $user], true),
            'options' => ['id' => 'userinfo'],
            'active' => $isProfile
        ],

    ],
]);

?>
    </div>
</div>

