<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\widgets\FileInput;
use yii\web\JsExpression;

$this->title = Yii::t('app', 'import.pagetitle');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-md-12">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'file')->widget(FileInput::classname(), [
        'name' => 'uploadPrice',
    	'options'=>[
        	//'multiple'=>true,
    		'showPreview' => false,
        ],
   		'pluginOptions' => [
            //'uploadUrl' => Url::to(['/catalog/import']),
            'browseLabel' => Yii::t('app', 'import.browseLabel'),
            'allowedFileExtensions' => ['xls' , 'xlsx' , 'csv'],
            'maxFileCount' => 1,
            'slugCallback' => new JsExpression('function(filename) {
                                return filename;
                                }'
            ),
    	]
    ]);

	Html::submitButton('Upload price', ['class' => 'btn btn-primary', 'name' => 'upload-button']);
?>
    </div>

<?php ActiveForm::end() ?>

<?php
if ($errors) {
    echo '<div class="col-md-6">';
        echo '<div class="panel panel-danger">';
            echo '<div class="panel-heading">';
                echo Html::tag('h3','Errors',['class'=>'panel-title']);
            echo '</div>';
            echo '<div class="panel-body">';
                for ($row = 0; $row <= count($errors) - 1; $row++) {
                    echo Html::tag('li',$errors[$row],['class'=>'list-group-item']);
                }
            echo '</div>';
        echo '</div>';
}

?>
</div>
