<div id="notify" class='notifications top-right'></div>
<div class="row">
    <div class="col-md-12">
        <table id="translates-table"
               data-toggle="table"
               data-url="/catalog/data"
               data-query-params="queryParams"
               data-show-columns="true"
               data-pagination="true"
               data-search="true"
               data-show-refresh="true"
               data-show-export="true"
               data-page-size="20"
               data-page-list="[10, 20, 50, 100]"
            >
            <thead>
            <tr>
                <th data-field="nursery" data-sortable="true" data-formatter="searchFormatter.nursery" ><?= Yii::t('app', 'field.nursery') ?></th>
                <th data-field="region" data-sortable="true" data-formatter="searchFormatter.region" ><?= Yii::t('app', 'field.region') ?></th>
                <th data-field="landing_type" data-sortable="true"><?= Yii::t('app', 'field.landing_type') ?></th>
                <th data-field="height_min" data-sortable="true" data-formatter="searchFormatter.height" data-align="center"><?= Yii::t('app', 'field.height') ?></th>
                <th data-field="width_min" data-sortable="true" data-formatter="searchFormatter.width" data-align="center"><?= Yii::t('app', 'field.width') ?></th>
                <th data-field="girth_min" data-sortable="true" data-formatter="searchFormatter.girth" data-align="center"><?= Yii::t('app', 'field.girth') ?></th>
                <th data-field="shtamb" data-sortable="true"><?= Yii::t('app', 'field.shtamb') ?></th>
                <th data-field="amount" data-sortable="true"><?= Yii::t('app', 'field.amount') ?></th>
                <th data-field="price" data-sortable="true" data-align="right"><?= Yii::t('app', 'field.price') ?></th>
                <th data-field="price_ds1" data-sortable="true" data-align="right"><?= Yii::t('app', 'field.price_ds1') ?></th>
                <th data-field="price_ds2" data-sortable="true" data-align="right"><?= Yii::t('app', 'field.price_ds2') ?></th>
                <th data-field="price_retail" data-sortable="true" data-align="right"><?= Yii::t('app', 'field.price_retail') ?></th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="input-group">
        </div>
    </div>
</div>

<div id="confirmation" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modal-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?= Yii::t('backend', 'delete.confirmation') ?>?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-primary" id="delete-confirm">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>


    var searchFormatter = function () {
        var self = {};


        self.region = function (value, row) {
            return row.nursery.region.name;
        };

        self.nursery = function (value, row) {
            return value.name;
        };

        self.height = function (value, row) {
            return value + row.height_max == 0 ? '' : value + '-' + row.height_max;
        };

        self.width = function (value, row) {
            return value + row.width_max == 0 ? '' : value + '-' + row.width_max;
        };

        self.girth = function (value, row) {
            return value + row.girth_max == 0 ? '' : value + '-' + row.girth_max;
        };

        return self;
    }();

    var russianText = '';

    function queryParams(params) {

        if(location.hash){
            var hash = location.hash;
            var filters = JSON.parse(hash.substr(1));
            $.extend(params, filters);
        }
        return params;
    }

    var lastHash = location.hash;
    setInterval(function () {
        if(lastHash != location.hash){
            lastHash = location.hash;
            $('#translates-table').bootstrapTable('refresh', {silent: true});
        }
    },100);

    function operateFormatter(value, row, index) {
        return [
            '<a class="edit ml10" href="javascript:void(0)" title="Edit">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            '<a class="remove ml10" href="javascript:void(0)" title="Remove">',
            '<i class="glyphicon glyphicon-remove"></i>',
            '</a>'
        ].join('');
    }

    var w = $('#myModal');


    $('#modal-form').unbind('submit').submit(function(){

        var data = $('#modal-form').serialize();

        $.ajax({
            url: '/catalog/data',
            dataType: 'json',
            data: data,
            type: 'post',
            success: function(){
                w.modal('hide');
                $('#translates-table').bootstrapTable('refresh', {silent: true});

                $('#notify').notify({
                    message: { html: '&nbsp;&nbsp;&nbsp;&nbsp; <b>ОК</b> Запись сохранена! &nbsp;&nbsp;&nbsp;&nbsp; ' },
                    fadeOut: { enabled: true, delay: 3000 }
                }).show();
            }
        });

        return false;
    });


    window.operateEvents = {

        'click .edit': function (e, value, row, index) {
            for(var ind in row) {
                $('#modal-form input[name="row['+ind+']"]').val(row[ind]);
            }
            w.modal('show');
        },

        'click .remove': function (e, value, row, index) {

            e.preventDefault();
            $('#confirmation').data('id', row.id).modal('show');

        }
    };

    $('#add-link').unbind('click').click(function(){

        $('#modal-form input').val('');
        w.modal('show');
        return false;
    });



</script>
