<?php
use kartik\tree\TreeView;
use common\models\Product;
use yii\helpers\Html;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
$this->title = 'My Yii Application';
$this->registerJsFile('js/index.js');
?>
<div class="site-index">
    <?php
    if(Yii::$app->user->isGuest):
    ?>
    <div class="jumbotron">

        <div id="index-logo"></div>

        <h1><?= Yii::t('app', 'index.welcome.title') ?></h1>

        <p class="lead"><?= Yii::t('app', 'index.welcome.message') ?></p>

    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">

                <div class="panel panel-default same-height">
                    <div class="panel-body">
                        <h2><?= Yii::t('app','index.login.title') ?></h2>

                        <p><?= Yii::t('app','index.login.message') ?></p>
                        <p>
                            <?=
                                Html::button(Yii::t('app', 'link.login'),
                                    [
                                        'value' => \yii\helpers\Url::to('site/login'),
                                        'class' => 'btn btn-primary',
                                        'id' => 'modalButton'
                                    ]);
                            ?>
                        </p>
<!--                        <p><a class="btn btn-primary" id="modalButton" href="/site/login">--><?//= Yii::t('app', 'link.login') ?><!--</a></p>-->

                    </div>

            </div>


            </div>
            <div class="col-lg-4">
                <div class="panel panel-default same-height">
                    <div class="panel-body">
                <h2><?= Yii::t('app','index.signup.title') ?></h2>

                <p><?= Yii::t('app','index.signup.message') ?></p>

                <p><a class="btn btn-primary" href="/site/signup"><?= Yii::t('app', 'link.signup') ?></a></p>
                    </div>
                </div>

            </div>
            <div class="col-lg-4">
                <div class="panel panel-default same-height">
                    <div class="panel-body">
                <h2><?= Yii::t('app','index.contact.title') ?></h2>

                <p><?= Yii::t('app','index.contact.message') ?></p>

                <p><a class="btn btn-primary" href="/site/contact"><?= Yii::t('app', 'link.contact') ?></a></p>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </div>
        </div>


    </div>
    <?php else: ?>
        <!--  Passing treeLang variable to main.js      -->
        <input type="hidden" id="treelang" value="<?php echo $treeLang; ?>" />
        <div class="row top-buffer">
            <div class="col-sm-3">
                <?=
                    Html::button('Lat' , ['id' => 'lat_button', 'class' => 'btn btn-primary','onClick' => 'changeTreeLang(event)', 'style' => $activeLat]);
                ?>
                <?=
                    Html::button('Ru' , ['id' => 'ru_button', 'class' => 'btn btn-primary','onClick' => 'changeTreeLang(event)', 'style' => $activeRu]);
                ?>
                <?=
                    Html::button('Ua' , ['id' => 'ua_button' , 'class' => 'btn btn-primary','onClick' => 'changeTreeLang(event)', 'style' => $activeUa]);
                ?>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-md-12">
                <?php
                    $query = Product::find()->addOrderBy('root, lft');
                    echo TreeView::widget([
                        'query' => $query,
                        'headingOptions' => ['label' => Yii::t('tree','main.tree.header')],
                        'options' => [
                            'id' => 'myTree',
                        ],
                        'nodeView' => '@frontend/views/site/_treeview_table',
                        'fontAwesome' => false,
                        'isAdmin' => false,
                        'toolbar' => [
                            'create' => false,
                            'create-root' => false,
                            'remove' => false,
                            'move-up' => false,
                            'move-down' => false,
                            'move-left' => false,
                            'move-right' => false,
                        ],
                        'displayValue' => 1,
                        'softDelete' => true,
                        'cacheSettings' => ['enableCache' => true]
                    ]);
                ?>
            </div>
        </div>

    <?php endif; ?>
</div>
<?php
Modal::begin([
    'header' => Yii::t('app', 'index.login.title'),
    'id' => 'modal',
    'size' => 'modal-sm',
    //'toggleButton' => ['label' => 'click me'],
]);
echo "<div id='modalContent'>";

Modal::end();
?>

