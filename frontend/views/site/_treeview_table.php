<?php
/**
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2015
 * @package   yii2-tree-manager
 * @version   1.0.3
 */

use common\models\ItemsSearch;
use common\models\Product;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
use kartik\tree\TreeView;

/**
 * @var yii\web\View            $this
 * @var kartik\tree\models\Tree $node
 * @var kartik\form\ActiveForm  $form
 */
?>

<?php
/**
 * SECTION 1: Initialize node view params & setup helper methods.
 */
?>
<?php

extract($params);
$isAdmin = ($isAdmin == true || $isAdmin === "true"); // admin mode flag
$inputOpts = [];                                      // readonly/disabled input options for node
$flagOptions = ['class' => 'kv-parent-flag'];         // node options for parent/child
$module = TreeView::module();
if (Yii::$app->session->get('treelang')) {
    $treelang = 'name_' . Yii::$app->session->get('treelang');
}
else{
    $treelang = 'name_lat';
}

/* Find product by node->id passed from index.php */
$product = Product::findOne(['id' => $node->id]);

/* Find parents of this product for breadcumbs */
$parents = $product->parents()->all();
$parents = (ArrayHelper::toArray($parents));
$parents = ArrayHelper::getColumn($parents, $treelang);
$parents[] = $node->$treelang;


echo Breadcrumbs::widget([
    'homeLink' => false,
    'links' => $parents,
]);

echo Html::tag('h1', $node->$treelang, ['class' => '']).'<br>';

/* Find leaves of product to pass their ids to ItemsSearch */
$leaves = $product->leaves()->all();
if ($leaves) {
    $leaves = (ArrayHelper::toArray($leaves));
    $leaves = ArrayHelper::getColumn($leaves, 'id');
}
else{
    $leaves = ArrayHelper::toArray($node->id);
}

$searchModel = new ItemsSearch();
$dataProvider = $searchModel->search([$searchModel['plant_id'] = $leaves]);
$dataProvider->sort = false;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'plant_id',
        'product.name_lat',
        'landing_type',
        'nursery_id',
        'nurseries.name',
        'price',
        [
            'attribute' => 'created_at',
            'format' => ['date', 'php:d/m/Y'],
        ],

    ],
]); ?>


