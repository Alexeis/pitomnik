/**
 * Created by user on 12.03.2016.
 */

$( document ).ready(function() {
    var treeLang = $("#treelang").val();
    var selected_button_id ="#" + treeLang+'_button';
    $( selected_button_id ).css( "box-shadow", "inset 0 0 0 1px #27496d,inset 0 5px 30px #193047" );
});

function changeTreeLang(event){
    var sel_button_id = $(event.target).attr("id");
    switch (sel_button_id) {
        case "lat_button":
            treelang = 'lat';
            break;
        case "ru_button":
            treelang = 'ru';
            break;
        case "ua_button":
            treelang = 'ua';
            break;
    }
    $.ajax({
        url: "site/treelang",
        data: { treelang_in: treelang }
    })
        .success(function() {
            location.reload(true);
        });
}

$('#modalButton').click(function(){
    $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
});

