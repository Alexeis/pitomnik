<?php
namespace app\components;

use common\models\PlantClasses;
use common\models\Plants;
use common\models\PlantSorts;
use yii\base\Object;
use yii\helpers\Html;
use common\models\PlantSpecies;


class SideTree extends Object {


    public function render()
    {

        $str = '';

        $plantClasses = PlantClasses::find()->all();

        foreach((array) $plantClasses as $plantClass){
            $str .=
                '<li><a href="#">' . $plantClass->name_ru . '</a>'
                . $this->renderPlantSort($plantClass->id)
                .'</li>';
        }

        return $str;
    }

    public function renderPlantSort($class_id)
    {

        $str = '';

        $plantSorts = PlantSorts::find()->where(['class_id' => $class_id])->all();

        if($plantSorts){
            $str .= '<ul>';

            foreach((array) $plantSorts as $plantSort){
                $str .=
                    '<li><a href="#">' . $plantSort->name_ru . '</a>'
                    . $this->renderPlants($plantSort->id)
                    .'</li>';
            }
            $str .= '</ul>';
        }

        return $str;
    }

    public function renderPlants($sort_id)
    {

        $str = '';

        $plants = Plants::find()->where(['sort_id' => $sort_id])->all();

        if($plants){
            $str .= '<ul>';

            foreach((array) $plants as $plant){
                $str .=
                    '<li><a href="/catalog/search#'.Html::encode(json_encode(['plant_id' => $plant->id])).'" class="nowrap"><small>' .PlantSpecies::findOne(['id'=>$plant->species_id])->name_ru . ' ' . ($plant->cultivar ? "({$plant->cultivar}) " : '') .  '</a>'
                    .'</small></li>';
            }
            $str .= '</ul>';
        }

        return $str;

    }



}