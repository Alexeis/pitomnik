<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;
use yii\captcha\Captcha;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required', 'message' => '{attribute} '.Yii::t('app', 'login.required.message')],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'signup.username.unique')],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => '{attribute} '.Yii::t('app', 'login.required.message')],
            ['email', 'email', 'message' => '{value} '.Yii::t('app', 'signup.email.invalid')],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'signup.email.unique')],

            ['password', 'required', 'message' => '{attribute} '.Yii::t('app', 'login.required.message')],
            ['password', 'string', 'min' => 6, 'tooShort' => '{attribute} '.Yii::t('app', 'signup.password.minlength')],

       		// verifyCode needs to be entered correctly
       		['verifyCode', 'captcha', 'message' => Yii::t('app', 'captcha.invalid')],
        		
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
    
    public function attributeLabels()
    {
    	return [
    			'username' => Yii::t('app', 'signup.username'),
    			'email' => Yii::t('app', 'signup.email'),
    			'password' => Yii::t('app', 'signup.password'),
    			'verifyCode' => Yii::t('app', 'contact.captcha'),
    	];
    }
}
