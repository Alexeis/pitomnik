<?php
namespace frontend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
	/**
	 * @var UploadedFile file attribute
	 */
	public $file;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
				[['file'], 'file', 'maxSize' => 1024 * 1024 * 5, 
						'tooBig' => '{attribute}' . Yii::t('app', 'upload.maxsize') . '{value}',
						'extensions'=>'xls, xlsx, csv',
						'checkExtensionByMimeType'=>false,
				],
						
		];
	}
}