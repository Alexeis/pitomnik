<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required', 'message' => '{attribute} '.Yii::t('app', 'login.required.message')],
            // email has to be a valid email address
            ['email', 'email', 'message' => '{value} '.Yii::t('app', 'signup.email.invalid')],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha', 'message' => Yii::t('app', 'captcha.invalid')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
    	return [
    			'name' => Yii::t('app', 'contact.name'),
    			'email' => Yii::t('app', 'contact.email'),
    			'subject' => Yii::t('app', 'contact.subject'),
    			'body' => Yii::t('app', 'contact.body'),
    			'verifyCode' => Yii::t('app', 'contact.captcha'),
    	];
    }
    
    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }
    
    
}
