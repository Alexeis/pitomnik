<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Nurseries;
use common\models\User;


/**
 * Site controller
 */
class ProfileController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionEdit()
    {
        $user = \Yii::$app->user->getIdentity();
        $nursery = $user->getNursery($user->id);

        if(!$nursery){
            $nursery = new Nurseries();
            $nursery->user_id = $user->id;
        }

        $isProfile = false;

        if (Yii::$app->request->post('Nurseries') && $nursery->load(Yii::$app->request->post()) && $nursery->save()) {
           Yii::$app->session->setFlash('success', Yii::t('app', 'nursery.update.success'), true);
        } elseif (Yii::$app->request->post('User') && $user->load(Yii::$app->request->post()) && $user->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'profile.update.success'), true);
            $isProfile = true;
        }

        return $this->render('edit', [
            'user' => $user,
            'nursery' => $nursery,
            'isProfile' => $isProfile
        ]);

    }

}