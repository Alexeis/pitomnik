<?php
namespace frontend\controllers;

use common\models\Items;
use Yii;
use yii\web\Controller;
use frontend\models\UploadForm;
use yii\web\UploadedFile;
use common\models\User;
use common\models\Plants;
use common\models\PlantClasses;
use common\models\StatItems;
use common\models\PlantSpecies;
use yii\web\ForbiddenHttpException;

/**
 * Site controller
 */
class CatalogController extends Controller
{
    /**
     * @var array of errors while import process
     */



    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout', 'signup'],
//                'rules' => [
//                    [
//                        'actions' => ['signup'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {

    }

    public function actionSearch()
    {

        return $this->render('search');
    }

    public function actionData()
    {
        \Yii::$app->response->format = 'json';

        $plant_id = Yii::$app->request->get('plant_id',0);

        $params = [];

        if($plant_id){
            $params['plant_id'] = $plant_id;
        }
        $items = Items::find()
            ->with('nursery.region')
            ->where($params)
            ->orderBy('id')->asArray()->all();
        return $items;
    }

    public function actionImport()
    {
        if (Yii::$app->user->can('importPrice')) {
            $model = new UploadForm();
            $errors = [];
            if (Yii::$app->request->isPost) {
                $model->file = UploadedFile::getInstance($model, 'file');
                if ($model->file && $model->validate()) {

                    $inputFileName = $model->file->tempName;
                    try {
                        $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
                        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    }
                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestRow();
                    $highestColumn = $sheet->getHighestColumn();


                    $currentNursery = User::getNursery(\Yii::$app->user->id);
                    if (!isset($currentNursery)){

                        $errors[] = 'You have no nursery';
                        return $this->render('import', ['model' => $model, 'errors' => $errors]);

                    }
                    //delete items from this nursery
                    Items::deleteAll("nursery_id = :nursery_id", ['nursery_id' => $currentNursery->id]);

                    //delete from statitems if user had loaded his price previously this day
                    date_default_timezone_set('Europe/Kiev');
                    $currTimestamp = getdate(time());
                    StatItems::deleteAll(['YEAR(FROM_UNIXTIME(created_at))' => $currTimestamp['year'],
                        'MONTH(FROM_UNIXTIME(created_at))' => $currTimestamp['mon'],
                        'DAY(FROM_UNIXTIME(created_at))' => $currTimestamp['mday'],
                        'nursery_id' => $currentNursery->id,
                    ]);

                    if (!file_exists(Yii::getAlias('@app/runtime/logs/importlogs/'))) {
                        mkdir(Yii::getAlias('@app/runtime/logs/importlogs/'), 0777, true);
                    }
                    $errorLog = iconv('UTF-8', 'windows-1251', $currentNursery->name) . date('dmY') . '.errorlog';
                    $logFile = fopen(Yii::getAlias('@app/runtime/logs/importlogs/') . $errorLog, 'wt');
                    fwrite($logFile, "Starting import from " . $currentNursery->name . " " . date('d/m/Y') . "\r\n");

                    //  Loop through each row of the worksheet in turn
                    for ($row = 1; $row <= $highestRow; $row++) {
                        //  Read a row of data into an array
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                            NULL,
                            FALSE,
                            FALSE);
                        /**
                         * $inputRecord is an array of Excel data file row
                         * $inputRecord[0][0] - plant_class name_lat, name_ru, name_ua
                         * $inputRecord[0][1] - plants name_lat, name_ru, name_ua
                         * $inputRecord[0][2] - plants cultivar
                         * $inputRecord[0][3] - items landing_type
                         * $inputRecord[0][4] - items height_min, height_max
                         * $inputRecord[0][5] - items width_min, width_max
                         * $inputRecord[0][6] - items girth_min, girth_max
                         * $inputRecord[0][7] - items shtamb
                         * $inputRecord[0][8] - items amount
                         * $inputRecord[0][9] - items price
                         *
                         */

                        $item = new Items();
                        $statItem = new StatItems();
                        $item->nursery_id = $statItem->nursery_id = $currentNursery->id;

                        $plantClass = PlantClasses::getPlantClassByName(trim($rowData[0][0]));
                        if (!$plantClass) {
                            $errors[] = $rowData[0][0] . ' - Plant Class doesnt exist';
                            continue;
                        }


                        $plant_specie = PlantSpecies::getPlantSpeciesByName(trim($rowData[0][1]));
                        if (!$plant_specie) {
                            $errors[] = $rowData[0][1] . ' Plant Specie doesnt exist';
                            continue;
                        } elseif(!(Plants::find()->AndWhere(['is', 'cultivar', null])->AndWhere(['species_id'=>$plant_specie->id])->one())
                                &&
                            (trim($rowData[0][2]) == '')){
                                //if there are not any plants with empty cultivar and cultivar field in import file is empty
                                $errors[] = 'There is no plant with empty cultivar. Please fill cultivar before next import';
                                continue;
                        }
                        // If there is any plant with cultivar type as in import file
                        if ((trim($rowData[0][2]) != '') && (!Plants::findOne(['cultivar' => $rowData[0][2],
                                                                            'species_id' => $plant_specie->id])))
                        {
                            $errors[] = $rowData[0][1] . ' Doesnt have this cultivar ' . $rowData[0][2];
                            continue;
                        }

                        $item->plant_id = $statItem->plant_id = Plants::findOne(['cultivar' => $rowData[0][2],
                                                                                 'species_id'=>$plant_specie->id

                                                                                ])->id;
                            //Plants::find()->Where(['cultivar' => $rowData[0][2]])->andWhere(['name_lat' => $rowData[0][1]])->one()->id;

                        $item->landing_type = $statItem->landing_type = $rowData[0][3];

                        if (isset($rowData[0][4])) {
                            $hminmax = preg_split("/(\/|\?|=|-)/", $rowData[0][4]);
                            if (is_numeric($hminmax[0]) && is_numeric($hminmax[1])) {
                                $item->height_min = $statItem->height_min = $hminmax[0];
                                $item->height_max = $statItem->height_max = $hminmax[1];
                            } else {
                                $errors[] = $rowData[0][4] . ' incorrect height';
                                continue;
                            }
                        } else {
                            $item->height_min = $statItem->height_min = 0;
                            $item->height_max = $statItem->height_max = 0;
                        }

                        if (isset($rowData[0][5])) {
                            $wminmax = preg_split("/(\/|\?|=|-)/", $rowData[0][5]);
                            if (is_numeric($wminmax[0]) && is_numeric($wminmax[1])) {
                                $item->width_min = $statItem->width_min = $wminmax[0];
                                $item->width_max = $statItem->width_max = $wminmax[1];
                            } else {
                                $errors[] = $rowData[0][5] . ' incorrect width';
                                continue;
                            }
                        } else {
                            $item->width_min = $statItem->width_min = 0;
                            $item->width_max = $statItem->width_max = 0;
                        }

                        if (isset($rowData[0][6])) {
                            $gminmax = preg_split("/(\/|\?|=|-)/", $rowData[0][6]);
                            if (is_numeric($gminmax[0]) && is_numeric($gminmax[1])) {
                                $item->girth_min = $statItem->girth_min = $gminmax[0];
                                $item->girth_max = $statItem->girth_max = $gminmax[1];
                            } else {
                                $errors[] = $rowData[0][6] . ' incorrect girth';
                                continue;
                            }
                        } else {
                            $item->girth_min = $statItem->girth_min = 0;
                            $item->girth_max = $statItem->girth_max = 0;
                        }

                        $item->shtamb = $statItem->shtamb = $rowData[0][7];

                        $item->amount = $statItem->amount = $rowData[0][8];

                        $item->price = $statItem->price = $rowData[0][9];

                        if (!$item->validate() && !$statItem->validate()) {
                            $errors = array_merge($errors, $item->errors, $statItem->errors);
                            continue;
                        }

                        $item->save();
                        $statItem->save();

                    }

                    //write log file
                    fwrite($logFile, count($errors) . " errors\r\n");
                    for ($row = 0; $row <= count($errors) - 1; $row++) {
                        fwrite($logFile, $errors[$row] . "\r\n");
                    }
                    fclose($logFile);
                }
            }

            return $this->render('import', ['model' => $model, 'errors' => $errors]);
        }
        else{
            throw new ForbiddenHttpException('Access denied');
        }
    }

}
